import os

from django.urls import re_path
from django.conf.urls.i18n import i18n_patterns
from django.contrib import admin
from django.contrib.sitemaps import views as sitemaps_views
from django.contrib.staticfiles.storage import staticfiles_storage
from django.urls import include, path
from django.views.generic.base import RedirectView
from fluent_blogs.sitemaps import *
from fluent_pages.sitemaps import *
from rpc4django.views import serve_rpc_request

from django_company_cms_about_us.django_company_cms_about_us.sitemaps import *
from django_company_cms_career.django_company_cms_career.sitemaps import *
from django_company_cms_cloud.django_company_cms_cloud.sitemaps import *
from django_company_cms_general.django_company_cms_general.sitemaps import *
from django_company_cms_isp.django_company_cms_isp.sitemaps import *
from django_company_cms_msp.django_company_cms_msp.sitemaps import *
from django_company_cms_partner.django_company_cms_partner.sitemaps import *
from django_company_cms_uc.django_company_cms_uc.sitemaps import *
from django_company_cms_voip.django_company_cms_voip.sitemaps import *
from django_company_cms_web.django_company_cms_web.sitemaps import *

handler400 = "django_company_cms_general.django_company_cms_general.views.bad_request"
handler403 = (
    "django_company_cms_general.django_company_cms_general.views.permission_denied"
)
handler404 = (
    "django_company_cms_general.django_company_cms_general.views.page_not_found"
)
handler500 = "django_company_cms_general.django_company_cms_general.views.server_error"

sitemaps = {
    # ABOUT US
    "about_us_views": AboutUsStaticViewSitemap,
    # BLOG
    "blog_entries": EntrySitemap,
    "blog_categories": CategoryArchiveSitemap,
    "blog_authors": AuthorArchiveSitemap,
    "blog_tags": TagArchiveSitemap,
    # CAREER
    "career_views": CareerStaticViewSitemap,
    # CLOUD
    "cloud_views": CloudStaticViewSitemap,
    # GENERAL
    "general_views": GeneralStaticViewSitemap,
    # ISP
    "isp_views": IspStaticViewSitemap,
    "isp_availability_check_countries": IspAvailabilityCheckCountriesSitemap,
    "isp_availability_check_cities": IspAvailabilityCheckCitiesSitemap,
    "isp_public_wlan_countries": IspPublicWlanCountriesSitemap,
    "isp_public_wlan_cities": IspPublicWlanCitiesSitemap,
    # MSP
    "msp_views": MspStaticViewSitemap,
    "msp_topics": MspTopicSitemap,
    # PARTNER
    "partner_views": PartnerStaticViewSitemap,
    # UC
    "uc_views": UcStaticViewSitemap,
    # VOIP
    "voip_views": VoipStaticViewSitemap,
    "voip_national_codes": VoipNationalCodeDetailsSitemap,
    # WEB
    "web_views": WebStaticViewSitemap,
    "web_top_level_domains": WebTopLevelDomainsSitemap,
    "web_top_level_domains_xn": WebTopLevelDomainsPunycodeSitemap,
}

urlpatterns = [
    re_path(r"", include("user_sessions.urls", "user_sessions")),
    path("tinymce/", include("tinymce.urls")),
    path("files/", include("db_file_storage.urls")),
    path("rpc", serve_rpc_request, name="rpc"),
    path("xml-rpc", serve_rpc_request, name="xml-rpc"),
    path("xmlrpc.php", serve_rpc_request, name="xmlrpc.php"),
    path("json-rpc", serve_rpc_request, name="json-rpc"),
    path("admin/", admin.site.urls),
    re_path("^i18n/", include("django.conf.urls.i18n")),
    re_path(r"^watchman/", include("watchman.urls")),
    re_path(r"^robots\.txt", include("robots.urls")),
    re_path(
        r"^security\.txt",
        include("django_mdat_security_txt.django_mdat_security_txt.urls"),
    ),
    re_path(
        r"^\.well-known/security\.txt",
        include("django_mdat_security_txt.django_mdat_security_txt.urls"),
    ),
    # re_path(r'^admin/util/taggit_autocomplete_modified/', include('taggit_autocomplete_modified.urls')),
]

# Dynamic loading of Favicon urls
for name in os.listdir(
    settings.PROJECT_ROOT
    + "/../django_customization_dolphinit/django_customization_dolphinit/static/company/favicon"
):
    urlpatterns.append(
        re_path(
            name,
            RedirectView.as_view(
                url=staticfiles_storage.url("company/favicon/" + name),
                permanent=False,
            ),
        )
    )

urlpatterns += i18n_patterns(
    path("sitemap.xml", sitemaps_views.index, {"sitemaps": sitemaps}),
    path(
        "sitemap-<section>.xml",
        sitemaps_views.sitemap,
        {"sitemaps": sitemaps},
        name="django.contrib.sitemaps.views.sitemap",
    ),
    # re_path(r'', include('fluent_pages.urls')),
    re_path(r"", include("django_company_cms_general.django_company_cms_general.urls")),
    path("mdat/location/", include("django_mdat_location.django_mdat_location.urls")),
    path("mdat/tld/", include("django_mdat_tld.django_mdat_tld.urls")),
)

# List modules that should not auto-map path
ignore_modules = [
    "django_company_cms_general",
    "django_company_cms_main",
    "django_company_cms_tasks",
]

# Dynamic loading of CMS urls
for name in os.listdir(settings.PROJECT_ROOT + "/.."):
    if os.path.isdir(name) and name.startswith("django_company_cms_"):
        if name in ignore_modules:
            continue

        module_name = name + "." + name
        url_path = name.replace("django_company_cms_", "").replace("_", "-")

        urlpatterns += i18n_patterns(
            path(url_path + "/", include(module_name + ".urls")),
        )

if settings.DEBUG:
    import debug_toolbar

    urlpatterns = [
        path("__debug__/", include(debug_toolbar.urls)),
    ] + urlpatterns
